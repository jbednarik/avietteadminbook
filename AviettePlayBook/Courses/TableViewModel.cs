﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Courses
{
    class TableViewModel
    {
        public TableViewModel()
        {
            Courses = new ObservableCollection<CourseDetailViewModel>();

            var query = (from s in StudentsRepository.Instance
                         from c in s.Courses
                         group c by new { c.Year, c.Name } into cGroups
                         select cGroups).OrderBy(x => x.Key.Year).ThenBy(x => x.Key.Name);


            foreach (var item in query)
            {
                var courseYear = item.Key.Year;
                var courseName = item.Key.Name;
                var students = StudentsRepository.Instance.Where(x => x.Courses.Any(c => c.Name == courseName && c.Year == courseYear)).ToArray();
                var courseViewModel = new CourseDetailViewModel
                {
                    Name = courseName,
                    Year = courseYear,
                    StudentsCount = students.Length,
                    Students = students,
                };
                Courses.Add(courseViewModel);
            }

            ShowDetailCommand = new RelayCommand(ShowDetail, CanShowDetail);
        }

        private bool CanShowDetail(object arg)
        {
            return SelectedCourse != null;
        }

        private void ShowDetail(object obj)
        {
            if (SelectedCourse == null)
                return;

            EventPublisher.Default.Publish(new MainWindowAggregateNavigationEventArgs
            {
                Path = new Uri("/Courses/List.xaml#" + SelectedCourse.Name, UriKind.Relative)
            });
        }

        public ObservableCollection<CourseDetailViewModel> Courses { get; set; }
        public CourseDetailViewModel SelectedCourse { get; set; }
        public RelayCommand ShowDetailCommand { get; set; }
    }
}
