﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Courses
{
    class CoursesViewModel : NotifyPropertyChanged
    {
        public CoursesViewModel()
        {
        }

        public CoursesViewModel(string name)
        {
            Courses = new ObservableCollection<CourseDetailViewModel>();

            var query = (from s in StudentsRepository.Instance
                         from c in s.Courses
                         where c.Name == name
                         group c by new { c.Year } into cGroup
                         select cGroup).OrderBy(x => x.Key.Year);

            foreach (var item in query)
            {
                var courseName = name;
                var year = item.Key.Year;

                var vm = new CourseDetailViewModel
                {
                    Name = courseName,
                    Year = year,
                    Students = StudentsRepository.Instance.Where(x => x.Courses.Any(c => c.Name == courseName)).ToArray()
                };
                Courses.Add(vm);
            }
        }

        public ObservableCollection<CourseDetailViewModel> Courses { get; set; }
    }
}
