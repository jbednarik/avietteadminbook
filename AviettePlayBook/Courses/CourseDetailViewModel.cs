﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Courses
{
    class CourseDetailViewModel
    {
        private Student[] students;
        public Student[] Students
        {
            get { return students; }
            set
            {
                students = value;
                StudentViewModels = new ObservableCollection<StudentViewModel>(
                    value.Select(x =>
                    {
                        var vm = new CourseDetailViewModel.StudentViewModel
                        {
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            Visits = (from c in x.Courses
                                      where c.Name == Name
                                      where c.Year == Year
                                      select c.TimesVisited).SingleOrDefault()
                        };
                        return vm;
                    }).OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToArray());
            }
        }

        public string Name { get; set; }
        public int StudentsCount { get; set; }
        public int Year { get; set; }
        public ObservableCollection<StudentViewModel> StudentViewModels { get; set; }
        public String DisplayName
        {
            get { return String.Format("{0} ({1})", Name, Year); }
        }

        internal class StudentViewModel
        {
            public String FirstName { get; set; }
            public String LastName { get; set; }
            public int Visits { get; set; }
        }
    }
}
