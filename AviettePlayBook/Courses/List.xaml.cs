﻿using FirstFloor.ModernUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AviettePlayBook.Courses
{
    /// <summary>
    /// Interaction logic for List.xaml
    /// </summary>
    public partial class List : UserControl, IContent
    {
        private static Uri selectedLink = null;

        public List()
        {
            InitializeComponent();
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Fragment))
                return;

            Tab.SelectedSource = new Uri("/Courses/CoursesView.xaml#" + e.Fragment, UriKind.Relative);
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            if (Tab.Links.Any(x => x.Source == selectedLink))
            {
                Tab.SelectedSource = selectedLink;
            }
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            foreach (var item in Tab.Links)
            {
                if (item.Source == Tab.SelectedSource)
                {
                    selectedLink = Tab.SelectedSource;
                    break;
                }
            }
        }
    }
}
