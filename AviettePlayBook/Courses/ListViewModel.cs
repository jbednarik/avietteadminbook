﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace AviettePlayBook.Courses
{
    class ListViewModel
    {
        public ListViewModel()
        {
            Courses = new LinkCollection();

            var query = (from c in StudentsRepository.Instance.SelectMany(x => x.Courses)
                         group c by c.Name into cGroups
                         select cGroups).OrderBy(x => x.Key);


            foreach (var item in query)
            {
                var courseName = item.Key;
                var c = new Link
                {
                    DisplayName = item.Key,
                    Source = new Uri("/Courses/CoursesView.xaml#" + courseName, UriKind.Relative)
                };

                Courses.Add(c);
            }
        }

        public LinkCollection Courses { get; set; }
    }
}
