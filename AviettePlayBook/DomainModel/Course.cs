﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook
{
    public class Course
    {
        public Course()
        {
        }

        public String Name { get; set; }
        public Int32 Year { get; set; }
        public Int32 TimesVisited { get; set; }
        public Boolean Finished { get; set; }
        public Note Note { get; set; }
    }
}
