﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AviettePlayBook
{
    class StudentsRepository : RepositoryBase<Student>
    {
        public static StudentsRepository Instance { get; set; }

        static StudentsRepository()
        {
            Instance = new StudentsRepository();
            try
            {
                Instance.Load();
            }
            catch (Exception)
            {
            }
            finally
            {
#if DEBUG
                if (Instance.Count == 0)
                {
                    Instance.Add(new Student
                    {
                        FirstName = "Jaroslav",
                        LastName = "Bednarik"
                    });
                    Instance[0].Courses.Add(new Course { Year = 2012, TimesVisited = 4 });
                    try
                    {
                        Instance.Save();
                    }
                    catch
                    {
                    }
                }
#endif
            }
        }
    }
}
