﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook
{
    public class Note
    {
        public Note()
        {
            Created = DateTime.Now;
        }

        public String Text { get; set; }
        public DateTime Created { get; set; }
        public Boolean Done { get; set; }
    }
}
