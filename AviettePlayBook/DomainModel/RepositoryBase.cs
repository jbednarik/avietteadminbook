﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AviettePlayBook
{
    class RepositoryBase<T> : ObservableCollection<T>
    {
        private readonly string filename;

        public RepositoryBase()
        {
            this.filename = typeof(T).Name + ".xml";
        }

        public void Save()
        {
            var arr = this.ToArray();

            XmlSerializer serializer = new XmlSerializer(arr.GetType());
            using (var fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                serializer.Serialize(fileStream, arr);
            }
        }

        public void Load()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T[]));
            using (var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                var players = (T[])serializer.Deserialize(fileStream);
                foreach (var item in players)
                {
                    this.Add(item);
                }
            }
        }
    }
}
