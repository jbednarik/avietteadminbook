﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook
{
    class NotesRepository : RepositoryBase<Note>
    {
        public static NotesRepository Instance { get; set; }

        static NotesRepository()
        {
            Instance = new NotesRepository();
            Instance.CollectionChanged += Instance_CollectionChanged;
            try
            {
                Instance.Load();
            }
            catch (Exception)
            {
            }
        }

        static void Instance_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ((NotesRepository)sender).Save();
        }
    }
}
