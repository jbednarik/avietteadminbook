﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AviettePlayBook
{
    public class Student
    {
        public Student()
        {
            Id = Guid.NewGuid();
            Courses = new CourseCollection();
        }

        public Guid Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public Gender Gender { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String ZipCode { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public Note Note { get; set; }
        public CourseCollection Courses { get; set; }
    }
}
