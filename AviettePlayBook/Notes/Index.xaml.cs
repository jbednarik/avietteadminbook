﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AviettePlayBook.Notes
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Index : UserControl
    {
        public Index()
        {
            InitializeComponent();
            Loaded += Index_Loaded;
            Unloaded += Index_Unloaded;
        }

        void Index_Unloaded(object sender, RoutedEventArgs e)
        {
            AppearanceManager.Current.PropertyChanged -= Current_PropertyChanged;
        }

        void Index_Loaded(object sender, RoutedEventArgs e)
        {
            AppearanceManager.Current.PropertyChanged += Current_PropertyChanged;
            UpdateStyles();
        }

        void Current_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ThemeSource")
            {
                UpdateStyles();
            }
        }

        private void UpdateStyles()
        {
            if (AppearanceManager.Current.ThemeSource.OriginalString.Contains("ModernUI.Dark.xaml"))
            {
                NotesItemsControl.ItemTemplate = (DataTemplate)this.Resources["DataTemplate3"];
            }
            else
            {
                NotesItemsControl.ItemTemplate = (DataTemplate)this.Resources["DataTemplate2"];
            }
        }
    }
}
