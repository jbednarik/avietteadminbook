﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Notes
{
    class NotesViewModel : NotifyPropertyChanged
    {
        public NotesViewModel()
        {
            AddCommand = new RelayCommand(Add);
            Notes = new ObservableCollection<NoteViewModel>();
            foreach (var item in NotesRepository.Instance.Reverse())
            {
                Notes.Add(new NoteViewModel(this, item));
            }
        }

        private void Add(object obj)
        {
            var note = new Note();
            NotesRepository.Instance.Add(note);
            Notes.Insert(0, new NoteViewModel(this, note));
        }

        public RelayCommand AddCommand { get; set; }
        public ObservableCollection<NoteViewModel> Notes { get; set; }

        internal class NoteViewModel : NotifyPropertyChanged
        {
            private Note note;
            private NotesViewModel owner;

            public NoteViewModel(NotesViewModel owner, Note note)
            {
                this.owner = owner;
                this.note = note;
                this.Text = note.Text;
                this.Created = note.Created;
                this.DeleteCommand = new RelayCommand(Delete);
                this.PropertyChanged += NoteViewModel_PropertyChanged;
            }

            public String Text { get; set; }
            public DateTime Created { get; set; }
            public RelayCommand DeleteCommand { get; set; }

            private void Delete(object obj)
            {
                owner.Notes.Remove(this);
                NotesRepository.Instance.Remove(note);
            }

            private void NoteViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
            {
                note.Text = Text;
                NotesRepository.Instance.Save();
            }
        }
    }
}
