﻿using FirstFloor.ModernUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AviettePlayBook.Students
{
    /// <summary>
    /// Interaction logic for List.xaml
    /// </summary>
    public partial class List : UserControl, IContent
    {
        private static Uri selectedIndex = null;
        private IDisposable eventSubscription;

        public List()
        {
            InitializeComponent();
            Loaded += List_Loaded;
            Unloaded += List_Unloaded;
        }

        void List_Unloaded(object sender, RoutedEventArgs e)
        {
            eventSubscription.Dispose();
        }

        void List_Loaded(object sender, RoutedEventArgs e)
        {
            eventSubscription = EventPublisher.Default.GetEvent<RefreshStudentLinks>().Subscribe(x =>
            {
                DataContext = new ListViewModel();
                if (x.Select != Guid.Empty)
                {
                    Tab.SelectedSource = new Uri("/Students/StudentDetail.xaml#" + x.Select.ToString(), UriKind.Relative);
                }
            });
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Fragment))
                return;

            if (e.Fragment == "new")
            {
                ((ListViewModel)this.DataContext).Students.Add(new FirstFloor.ModernUI.Presentation.Link()
                {
                    DisplayName = "New...",
                    Source = new Uri("/Students/StudentDetail.xaml#new", UriKind.Relative)
                });
                Tab.SelectedSource = new Uri("/Students/StudentDetail.xaml#new", UriKind.Relative);
            }
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            if (Tab.Links.Any(x => x.Source == selectedIndex))
            {
                Tab.SelectedSource = selectedIndex;
            }
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            foreach (var item in Tab.Links)
            {
                if (item.Source == Tab.SelectedSource)
                {
                    selectedIndex = Tab.SelectedSource;
                    break;
                }
            }
        }
    }
}
