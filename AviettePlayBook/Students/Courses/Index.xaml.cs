﻿using FirstFloor.ModernUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AviettePlayBook.Students.Courses
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Index : UserControl, IContent
    {
        private static Uri selectedLink = null;

        public Index()
        {
            InitializeComponent();
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Fragment))
                return;

            foreach (var item in Tab.Links)
            {
                if (item.Source.OriginalString.EndsWith(e.Fragment))
                {
                    Tab.SelectedSource = item.Source;
                    break;
                }
            }
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            if (Tab.Links.Any(x => x.Source == selectedLink))
            {
                Tab.SelectedSource = selectedLink;
            }
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            foreach (var item in Tab.Links)
            {
                if (item.Source == Tab.SelectedSource)
                {
                    selectedLink = Tab.SelectedSource;
                    break;
                }
            }
        }
    }
}
