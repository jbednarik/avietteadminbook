﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Students.Courses
{
    class StudentCoursesViewModel : NotifyPropertyChanged
    {
        public StudentCoursesViewModel()
        {
            StudentCourses = "Students' Courses";
        }

        public StudentCoursesViewModel(Student student)
        {
            this.Courses = student.Courses;

            StudentCourses = student.FirstName + " " + student.LastName + "'s Courses";
            SaveCommand = new RelayCommand(Save);
            DeleteSelectedCommand = new RelayCommand(DeleteSelected, CanDeleteSelected);
            ShowDetailCommand = new RelayCommand(ShowDetail, CanShowDetail);
        }


        public String StudentCourses { get; set; }
        public Course SelectedCourse { get; set; }
        public CourseCollection Courses { get; set; }
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand DeleteSelectedCommand { get; set; }
        public RelayCommand ShowDetailCommand { get; set; }

        private void Save(object obj)
        {
            StudentsRepository.Instance.Save();
        }

        private bool CanDeleteSelected(object arg)
        {
            return SelectedCourse != null;
        }

        private void DeleteSelected(object obj)
        {
            if (SelectedCourse == null)
                return;
            Courses.Remove(SelectedCourse);
        }

        private bool CanShowDetail(object arg)
        {
            return SelectedCourse != null;
        }

        private void ShowDetail(object obj)
        {
            EventPublisher.Default.Publish(new MainWindowAggregateNavigationEventArgs
            {
                Path = new Uri("/Courses/List.xaml#" + SelectedCourse.Name, UriKind.Relative)
            });
        }
    }
}
