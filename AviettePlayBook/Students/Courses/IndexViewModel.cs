﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Students.Courses
{
    class IndexViewModel
    {
        public IndexViewModel()
        {
            Students = new LinkCollection();

            foreach (var item in StudentsRepository.Instance.OrderBy(x => x.LastName).ThenBy(x => x.FirstName))
            {
                Students.Add(new Link
                {
                    Source = new Uri("/Students/Courses/StudentCourses.xaml#" + item.Id, UriKind.Relative),
                    DisplayName = item.LastName
                });
            }
        }

        public LinkCollection Students { get; set; }
    }
}
