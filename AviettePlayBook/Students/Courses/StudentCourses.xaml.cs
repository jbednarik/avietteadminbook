﻿using FirstFloor.ModernUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AviettePlayBook.Students.Courses
{
    /// <summary>
    /// Interaction logic for StudentCourses.xaml
    /// </summary>
    public partial class StudentCourses : UserControl, IContent
    {
        private static int selectedIndex = -1;

        public StudentCourses()
        {
            InitializeComponent();
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            var fragment = new Guid(e.Fragment);
            var student = StudentsRepository.Instance.FirstOrDefault(x => x.Id == fragment);
            if (student != null)
            {
                DataContext = new StudentCoursesViewModel(student);
            }
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            Grid.SelectedIndex = selectedIndex;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            selectedIndex = Grid.SelectedIndex;
        }
    }
}
