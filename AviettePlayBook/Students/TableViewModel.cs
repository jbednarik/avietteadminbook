﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Students
{
    class TableViewModel
    {
        public TableViewModel()
        {
            Students = StudentsRepository.Instance;
            SaveCommand = new RelayCommand(Save);
            ShowCoursesCommand = new RelayCommand(ShowCourses);
        }

        private void ShowCourses(object obj)
        {
            EventPublisher.Default.Publish(new MainWindowAggregateNavigationEventArgs
            {
                Path = new Uri("/Students/Courses/Index.xaml#" + SelectedItem.Id.ToString(), UriKind.Relative)
            });
        }

        private void Save(object obj)
        {
            StudentsRepository.Instance.Save();
        }

        public ObservableCollection<Student> Students { get; set; }
        public Student SelectedItem { get; set; }
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand ShowCoursesCommand { get; set; }
    }
}
