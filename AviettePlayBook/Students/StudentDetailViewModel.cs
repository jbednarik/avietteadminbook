﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Students
{
    class StudentDetailViewModel : NotifyPropertyChanged
    {
        private Student student;

        public StudentDetailViewModel()
        {
        }

        public StudentDetailViewModel(Student student)
        {
            this.student = student;
            this.FirstName = student.FirstName;
            this.LastName = student.LastName;
            this.IsMale = (student.Gender == Gender.Male) ? true : false;
            this.IsFemale = (student.Gender == Gender.Female) ? true : false;
            this.Email = student.Email;
            this.Phone = student.Phone;
            this.Address = student.Address;
            this.City = student.City;
            this.ZipCode = student.ZipCode;
            this.Note = student.Note ?? new Note();

            this.SaveCommand = new RelayCommand(Save, CanSave);
        }

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String ZipCode { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public Boolean IsMale { get; set; }
        public Boolean IsFemale { get; set; }
        public Note Note { get; set; }

        public RelayCommand SaveCommand { get; set; }

        private bool CanSave(object arg)
        {
            return true;
        }

        private void Save(object obj)
        {
            if (student == null)
                return;

            student.FirstName = this.FirstName;
            student.LastName = this.LastName;
            student.Gender = IsMale ? Gender.Male : Gender.Female;
            student.Email = this.Email;
            student.Phone = this.Phone;
            student.Address = this.Address;
            student.City = this.City;
            student.ZipCode = this.ZipCode;
            student.Note = this.Note;

            if (!StudentsRepository.Instance.Contains(student))
            {
                StudentsRepository.Instance.Add(student);
            }

            StudentsRepository.Instance.Save();
            EventPublisher.Default.Publish(new RefreshStudentLinks { Select = student.Id });
        }
    }
}
