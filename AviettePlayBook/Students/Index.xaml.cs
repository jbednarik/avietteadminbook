﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AviettePlayBook.Students
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Index : UserControl
    {
        public Index()
        {
            InitializeComponent();
            Loaded += Index_Loaded;
        }

        void Index_Loaded(object sender, RoutedEventArgs e)
        {
            if (!StudentsRepository.Instance.Any())
            {
                StudentsIndexTab.SelectedSource = new Uri("/Students/List.xaml", UriKind.Relative);
            }
        }
    }
}
