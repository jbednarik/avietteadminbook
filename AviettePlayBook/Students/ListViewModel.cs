﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace AviettePlayBook.Students
{
    class ListViewModel
    {
        public ListViewModel()
        {
            Students = new LinkCollection();

            foreach (var item in StudentsRepository.Instance.OrderBy(x => x.LastName).ThenBy(x => x.FirstName))
            {
                Students.Add(new Link
                {
                    Source = new Uri("/Students/StudentDetail.xaml#" + item.Id, UriKind.Relative),
                    DisplayName = item.LastName
                });
            }            
        }

        public LinkCollection Students { get; set; }
    }
}
