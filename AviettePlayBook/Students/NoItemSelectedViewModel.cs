﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AviettePlayBook.Students
{
    class NoItemSelectedViewModel
    {
        public NoItemSelectedViewModel()
        {
            AddCommand = new RelayCommand(Add);
        }

        public RelayCommand AddCommand { get; set; }

        private void Add(object obj)
        {
            EventPublisher.Default.Publish(new MainWindowAggregateNavigationEventArgs
            {
                Path = new Uri("/Students/List.xaml#new", UriKind.Relative)
            });
        }
    }
}
