﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Reactive.Linq;

namespace AviettePlayBook
{
    public class EventPublisher
    {
        private readonly ConcurrentDictionary<Type, object> subjects;

        public static EventPublisher Default { get; private set; }

        static EventPublisher()
        {
            Default = new EventPublisher();
        }

        public EventPublisher()
        {
            subjects = new ConcurrentDictionary<Type, object>();
        }

        public IObservable<TEvent> GetEvent<TEvent>()
        {
            var subject = (ISubject<TEvent>)subjects.GetOrAdd(typeof(TEvent), t => new Subject<TEvent>());
            return subject.AsObservable();
        }

        public void Publish<TEvent>(TEvent sampleEvent)
        {
            object subject;
            if (subjects.TryGetValue(typeof(TEvent), out subject))
            {
                ((ISubject<TEvent>)subject).OnNext(sampleEvent);
            }
        }
    }

    public class AggregateNavigationEventArgs
    {
        public Uri Path { get; set; }
    }

    public class MainWindowAggregateNavigationEventArgs
    {
        public Uri Path { get; set; }
    }
}
